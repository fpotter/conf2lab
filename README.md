# Conf2Lab

Confluence-to-GitLab conversion utility (WIP)

<a href="https://www.flaticon.com/free-icons/conversion" title="conversion icons">icon by cubydesign/flaticon</a>

## Usage/Development

Requires Python 3.11 to install. (Designed for PyPI but that will come later.)

```bash
make init
source .dev
poetry install
```

We bundled a little Docker Compose configuration to run Confluence and Postgres locally for trial exploration.

```bash
docker compose -f test-instance/docker-compose.yml build
```

It requires a Confluence Data Center license, so once it's up and running and you'll need to configure the instance in the UI. Use the `make` commands to start and stop the server.

(If someone can figure out the volume mappings so we keep the database, then `docker compose up` and `down` will work instead.)

```bash
make start-test-instance
```

Then [browse](http://localhost:8090/).

Add some spaces and content to your Confluence so you have something to work with. (And theoretically, it would work with a real Confluence Server instance.)

The `conf2lab` tool itself is designed to run as a CLI and makes use of [WizLib](https://gitlab.com/steampunk-wizard/wizlib) for configuration. We dropped a sample config file in the `test-instance` directory and even included an alias in `.dev` that runs the command until we clean things up. Make your own version of the alias to pull in your API token from somewhere (I use `op`, the 1Password CLI). I called my alias `test`.

Trust us, this will get simpler. Once you've got it all set up:

```bash
test get spaces
```

This will create a file `./test-stage/spaces/all.yml` with data about the spaces.

The other command expect space keys or content id's on stdin. So for example:

```bash
echo -n "BR\nAGG" | test get contents
```

... will populate `./test-stage/contents` with all kinds of metadata about content in the `BR` and `ARR` spaces. And:

```bash
echo -n "12345" | test get markup
```

... will populate an XML file (yes, that's how it comes) in `./test-stage/markup`.

The commands provide output on stdin that is designed to be input to another command, so feel free to chain them:

```bash
test get spaces | test get contents | test get markup
```

... will pull everything. Though of course in production you'll want to break it down a little, thus the separate commands.

Next step: Put some of that markup into a Git repo and push it to a GitLab Wiki.

**REMEMBER TO STOP YOUR INSTANCE!**

```bash
make start-test-instance
make stop-test-instance
```

## Resources

- [Differences between Confluence Data Center and Cloud](https://support.atlassian.com/migration/docs/differences-using-confluence-data-center-and-cloud/)
- [GitLab wiki capability](https://docs.gitlab.com/ee/user/project/wiki/)
- [Confluence Cloud v2 REST API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/)
- [Confluence storage format](https://confluence.atlassian.com/doc/confluence-storage-format-790796544.html)
- [Confluence Server REST API documentation](https://developer.atlassian.com/server/confluence/confluence-server-rest-api/)
- [Confluence Server REST API documentation](https://docs.atlassian.com/ConfluenceServer/rest/8.7.2/) - looks different

## Other Tools

Most seem to start with an export, some outdated

- [migrate-confluence tool](https://gitlab.oit.duke.edu/gf18/migrate-confluence) - in PHP. Dormant for a year.
- [Markdown exporter for Confluence](https://marketplace.atlassian.com/apps/1221351/markdown-exporter-for-confluence) - only works with Cloud.
- [Export to Markdown for Confluence Cloud](https://marketplace.atlassian.com/apps/1227287/export-to-markdown-for-confluence-cloud?tab=overview&hosting=cloud) - only works with Cloud.
- [Confluence to Markdown](https://github.com/EWhite613/Confluence-to-Github-Markdown) - Archived in 2023 and labeled "not active" - `.js`
- [Confluence to Markdown](https://github.com/meridius/confluence-to-markdown) - Fork of the above. Archived in 2021 - `.coffee`/`npm`

Notes about `migrate-confluence`:

- How to turn the `.mediawiki` files into a functioning wiki on GitLab?
- `migrate-confluence` requires or recommends some MediaWiki extensions - does GitLab include them?
    - TemplateStyles
    - ParserFunctions
    - Semantic MediaWiki
    - HeaderTabs

## Confluence capabilities

Some might differ from GitLab - so manage expectations

- A robust, purpose-designed page editing UI
- Separate "spaces"
- Granular access control (including page-level permissions)
- Granular notifications
- Analytics
- Macro (realtime scripting) capability
- Reconfigurable navigation
- Space style controls and page templates
- "Gadgets" for realtime data
- Calendar features
- A "Questions" feature
- An app marketplace - so virtually limitless integrations
- Deep integration with Jira
- Granular integrations with Slack, MS Teams, and Trello
- "Files" as first-class objects (sometimes not handled by `migrate-confluence`)
- Comments (not handled by `migrate-confluence`)
- Blog posts (not handled by `migrate-confluence`)


## Questions and considerations

Mostly notes to self

- Wiki vs Pages?
- What is a "category" in Confluence?
- How widely adopted are "user spaces"?
- Would it make sense to migrate each "space" to a separate Wiki?
- The Cloud API supports "custom content" separately from "pages". What is that, and does it apply?

## (Legacy) export support

We started working with export. A Confluence space XML export is actually a
Hibernate archive. Our first iteration is to see if we can parse the archive
file meaningfully.

Do something like this:

1. Install all this (venv etc - see Makefile)
1. Export a Confluence Cloud space to XML
1. Unzip it
1. `python -m conf2lab explore path/to/unzipped/directory`

Right now all it does is extract page titles. Example:

```bash
$ DEBUG=1 python -m conf2lab explore ./test-data/export-01
[{'title': 'Salvador Dali'}, {'title': 'Home'}, {'title': 'Home'}, {'title': 'Home'}, {'title': 'Art history Home'}]
Done
```

(yes, that example is included with the project.)


## Personal stuff

- [My test account](https://gitlab-fpotter-test.atlassian.net/)



