from unittest import TestCase
from tempfile import TemporaryDirectory
from pathlib import Path

from conf2lab.stage import Stage

class TestStage(TestCase):

    def test_save(self):
        with TemporaryDirectory() as dir:
            s = Stage(dir)
            s.store('f', {'g':'h'})
            with open(Path(dir) / 'f.yml') as file:
                v = file.read()
        self.assertEqual(v, 'g: h\n')

