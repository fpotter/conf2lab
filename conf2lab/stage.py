from dataclasses import dataclass
from pathlib import Path
import shutil
from xml.dom import minidom
from lxml import etree as ElementTree


from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


@dataclass
class Stage:
    """Local file storage for data in between pull and push"""

    dir: str

    @property
    def dirpath(self):
        if not hasattr(self, '_dirpath'):
            self._dirpath = Path(self.dir)
            if not self._dirpath.exists():
                self._dirpath.mkdir()
        return self._dirpath

    def store(self, dir:str, name:str, value, type='yml'):
        """Store data on the stage"""
        dir = (self.dirpath) / dir
        if not dir.exists():
            dir.mkdir()
        with open(dir / (name + '.' + type), 'w') as file:
            if type == 'yml':
                file.write(dump(value))
            elif type == 'xml':
                # dom = minidom.parseString(value)
                # file.write(dom.toprettyxml(indent="  "))

                parser = ElementTree.XMLParser(recover=True)
                tree = ElementTree.fromstring(value, parser)
                # root = ElementTree.Element('ConfluenceStorageFormat')
                # root.append(tree)
                # doc = ElementTree.ElementTree(root)
                string = ElementTree.tostring(tree, pretty_print=True)
                file.write(string.decode('utf-8'))

            else:
                file.write(value)

    def clean(self):
        shutil.rmtree(self.dirpath)

