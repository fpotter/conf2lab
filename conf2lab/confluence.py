from dataclasses import dataclass

import requests

@dataclass
class Confluence:

    hostname: str
    token: str

    def get(self, endpoint:str, params={}, multi=True):
        headers = {
            'Authorization': f"Bearer {self.token}",
            'Accept': 'application/json'
        }
        response = requests.get(f"http://{self.hostname}/rest/api/{endpoint}",
                headers=headers, params=params)
        if multi:
            result = response.json()['results']
            cleaned = [clean(d) for d in result]
        else:
            result = response.json()
            cleaned = clean(result)
        return cleaned

    def spaces(self):
        return self.get('space')
    
    def contents(self, spacekey:str):
        params = { 'spaceKey': spacekey }
        return self.get('content', params)

    def markup(self, contentid:str):
        params = { 'expand': 'body.storage' }
        return self.get(f"content/{contentid}", params, multi=False)

def clean(d):
    return {k:d[k] for k in d if not k.startswith('_')}