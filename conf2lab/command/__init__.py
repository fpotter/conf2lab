from wizlib.command import WizCommand
from wizlib.input_handler import InputHandler
from wizlib.config_handler import ConfigHandler


class Conf2LabCommand(WizCommand):

    handlers = [InputHandler, ConfigHandler]
