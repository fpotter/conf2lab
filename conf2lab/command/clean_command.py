from wizlib.parser import WizParser

from conf2lab.command import Conf2LabCommand
from conf2lab.stage import Stage

class CleanCommand(Conf2LabCommand):
    """Clean the stage for a fresh run"""

    name = 'clean'

    @Conf2LabCommand.wrap
    def execute(self):
        # TODO: abstract this
        directory = self.config.get('stage-directory')
        stage = Stage(directory)
        stage.clean()
        self.status = 'Cleaned'
