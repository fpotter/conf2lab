from wizlib.parser import WizParser

from conf2lab.command import Conf2LabCommand
from conf2lab.confluence import Confluence
from conf2lab.stage import Stage

class GetCommand(Conf2LabCommand):
    """Get data from a Confluence instance"""

    name = 'get'

    @classmethod
    def add_args(cls, parser: WizParser):
        super().add_args(parser)
        parser.add_argument('type', choices=['spaces', 'contents', 'markup'])

    @Conf2LabCommand.wrap
    def execute(self):
        token = self.config.get('confluence-token')
        hostname = self.config.get('confluence-hostname')
        directory = self.config.get('stage-directory')
        stage = Stage(directory)
        # TODO: Better error reporting
        api = Confluence(token=token, hostname=hostname)

        # TODO: OOP you idiot!
        if self.type == 'spaces':
            spaces = api.spaces()
            stage.store(self.type, 'all', spaces)
            output = '\n'.join([s['key'] for s in spaces])
        elif self.type == 'contents':
            spacekeylist = self.input.text.split('\n')
            output = ''
            for spacekey in spacekeylist:
                contents = api.contents(spacekey)
                stage.store(self.type, spacekey.lower(), contents)
                output += '\n'.join([c['id'] for c in contents])
        elif self.type == 'markup':
            contentidlist = self.input.text.split('\n')
            for contentid in contentidlist:
                result = api.markup(contentid)
                markup = result['body']['storage']['value']
                stage.store(self.type, str(contentid), markup, type='xml')
            output = '\n'.join(contentidlist)
        self.status = f"Got {self.type}"
        return output
            



