from pathlib import Path
from xml.sax import ContentHandler, make_parser


from wizlib.parser import WizParser

from conf2lab.command import Conf2LabCommand

ENTITIES_FILE = 'entities.xml'

class ExploreCommand(Conf2LabCommand):
    """Explore a Confluence export"""

    name = 'explore'

    @classmethod
    def add_args(cls, parser: WizParser):
        super().add_args(parser)
        parser.add_argument('directory')

    @Conf2LabCommand.wrap
    def execute(self):
        entitiespath = Path(self.directory) / ENTITIES_FILE
        handler = PageHandler()
        parser = make_parser()
        parser.setContentHandler(handler)
        with open(entitiespath) as entitiesfile:
            parser.parse(entitiesfile)

        self.status = "Done"
        return str(handler.pages)


class PageHandler(ContentHandler):
    def __init__(self):
        self.pages = []
        self.in_page = False
        self.property = None

    def startElement(self, name, attrs):
        if name == 'object' and attrs.get('class') == 'Page':
            self.in_page = True
            self.page = {}
        elif self.in_page and name == 'property' and attrs.get('name') == 'title':
            self.property = attrs.get('name')
            self.cdata = ''

    def endElement(self, name):
        if name == 'object' and self.in_page:
            self.in_page = False
            self.pages.append(self.page)
        elif name == 'property' and self.in_page and self.property:
            self.page[self.property] = self.cdata
            self.property = None

    def characters(self, content):
        if self.property:
            self.cdata += content

