from wizlib.app import WizApp

from conf2lab.command import Conf2LabCommand


class Conf2LabApp(WizApp):

    base_command = Conf2LabCommand
    name = 'conf2lab'

